
var http = require('http');
var url = require('url');
var target_dir = process.env.HOME+"/Downloads";

var server = http.createServer(function (request, response) {
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("Hello World\n");
	var url_parts = url.parse(request.url, true);
	var query = url_parts.query;
	console.log("File: "+query["filename"]+"\n");
	console.log("URL: "+query["url"]+"\n");
	var spawn = require('child_process').spawn,
    		aria = spawn("aria2c", ["-j 16", "-x 16", "-s 16", "-k 1M", "--check-certificate=false", "-d "+target_dir, query["url"]]);
	aria.stdout.on('data', function (data) {
		console.log('stdout: ' + data);
	});
	aria.on('close', function (code) {
		  console.log('child process exited with code ' + code);
	});
});

server.listen(2323);

