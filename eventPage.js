chrome.downloads.onCreated.addListener(function(downloadItem) {
  chrome.cookies.getAll({"url": downloadItem.url}, function(cookie) {
    console.log(cookie);
  });
  console.log('Receive download for URL ' + downloadItem.url);
  chrome.downloads.cancel(downloadItem.id, function() {
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:2323/?url=" + escape(downloadItem.url) + "&filename=" + escape(downloadItem.filename)
    console.log("GET to " + url)
    xhr.open("GET",url, true);
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
        console.log("Response from server: " + xhr.responseText)
      }
    }
    xhr.send();
    chrome.downloads.erase({
      id: downloadItem.id
    });
  });
});